//
//  main.m
//  project
//
//  Created by Sonam Maniar on 2/10/16.
//  Copyright © 2016 Sonam Maniar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
