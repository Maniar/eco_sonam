//
//  AppDelegate.h
//  project
//
//  Created by Sonam Maniar on 2/10/16.
//  Copyright © 2016 Sonam Maniar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

